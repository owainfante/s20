let number = Number(prompt("Please input a number"))

console.log("The number you provided is " + number + ".");

for(let i = number; i > 0; i--) {

    if(i <= 50) {
        console.log("The current value is at 50. Terminating Loop");
        break
    }
    if(i % 10 === 0){
        console.log("The number is divisible by 10. Skipping the number")
        continue
    }
    if(i % 5 === 0) {
        console.log(i);
        continue
    }
}

let val = "supercalifragilisticexpialidocious"
let consonants = ""

for (let i = 0; i < val.length; i++) {

    if(val[i] == "a" || val[i] == "e" || val[i] == "i" || val[i] == "o" || val[i] == "u"){
    continue
    }
    consonants = consonants + val[i]
}

console.log(val)
console.log(consonants)
